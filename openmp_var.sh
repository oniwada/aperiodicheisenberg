export OPENBLAS_NUM_THREADS=1
export OMP_NUM_THREADS=2
mpirun -x OMP_NUM_THREADS=4 --oversubscribe --bind-to none:overload-allowed -map-by ppr:2:socket:pe=4 ./runAH Results.db Results Inputs.txt res_
