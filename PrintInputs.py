import numpy as np

L = 178
ns = 5
Mbd = 500
mbd = 50
noise = 0
t_error = 1e-8
Jb = 1.0
r = 0.0
Delta = 0.875
NDis = 4

def PPrint(L, ns,Mbd, mbd, noise, t_error, Jb, rr, Delta, NDis):
	return f"{L} {ns} {Mbd} {mbd} {noise} {t_error} {Jb} {rr} {Delta} {NDis}"
def PPrint2(L, ns,Mbd, mbd, noise, t_error, Jb, rr, Delta, NDis):
	return f"{L} {ns} {Mbd} {mbd} {noise} {t_error} {Jb} {rr} {Delta}"

output1 = ''
output2 = ''

#[42, 86, 178, 366 ,754, 1552, 3194]
L_range = [1552]
r_range = np.arange(0.0, 1.0, 0.03125)
for L in L_range:
	for rr in r_range:
		output1 += PPrint(L, ns,Mbd, mbd, noise, t_error, Jb, rr, Delta, NDis)+"\n"
		output2 += f"Parameters_dict[{Delta}].append(\""+PPrint2(L, ns,Mbd, mbd, noise, t_error, Jb, rr, Delta, NDis)+"\")"+"\n"

print(output1)
print(output2)
