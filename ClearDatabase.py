import numpy as np
import matplotlib.pyplot as plt
import os, sys
from matplotlib import rc

rc('text', usetex=True)
rc('font', **{"size" : 8})
sys.path.insert(0, '../GeneralMonteCarlo')
import _SQLite_Database_ as SQLite

DatabaseName = sys.argv[2]+"/"+sys.argv[1]
FolderName = sys.argv[2]
Database = SQLite._SQLite_Database_(DatabaseName, "Results")
Tables = tuple(Database.Exec("SELECT name FROM sqlite_master WHERE type='table' AND name!='Results';"))

Database.Exec(f"DELETE FROM Results where id={sys.argv[3]};")
Database.Commit()

os.system(f'''rm -r {FolderName}/A_res*_{sys.argv[3]}''')
os.system(f'''rm -r {FolderName}/O_res*_{sys.argv[3]}''')
os.system(f'''rm -r {FolderName}/res*_{sys.argv[3]}''')
