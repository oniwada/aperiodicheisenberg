#include <iostream>
#include <iomanip> 
#include <string>
#include <vector>
#include <map>
#include <algorithm>

#include <mpi.h>
#include <omp.h>

#include "Parameters.h"
#include "itensor/all.h"
#include "ITensorDMRG.h"
#include "GenerateDisorder.h"
#include "_Database_.h"

int main(int Nargs, char* Inputs[]){
	
 int rank, size, thread_level;
 MPI_Init(&Nargs, &Inputs);
 MPI_Comm_rank(MPI_COMM_WORLD, &rank);
 MPI_Comm_size(MPI_COMM_WORLD, &size);

// _Database_<double> Db("Results.db", "Results");

 Parameters<double> Param;
 std::pair<std::string, std::vector<double>> p;
 p.second.push_back(10);
 
 p.first = "L";
 p.second[0] = 400;
 Param.m.insert(p);
 
 p.first  = "n_sweeps";
 p.second[0] = 10;
 Param.m.insert(p);
 
 p.first  = "max_bond_dim";
 p.second[0] = 400;
 Param.m.insert(p);

 p.first  = "min_bond_dim";
 p.second[0] = 50;
 Param.m.insert(p);
 
 p.first  = "t_error";
 p.second[0] = 1e-8;
 Param.m.insert(p);
 
 p.first  = "noise";
 p.second[0] = 1e-8;
 Param.m.insert(p);

 p.first  = "Jb";
 p.second[0] = 1;
 Param.m.insert(p);
 
 p.first  = "NDis";
 p.second[0] = 5;
 Param.m.insert(p);

 p.first  = "r";
 p.second[0] = 0.125;
 Param.m.insert(p);
 
 p.first = "Delta";
 p.second[0] = 1;
 Param.m.insert(p);
 
 p.first  = "ID";
 p.second[0] = 1;
 Param.m.insert(p);
 
 std::cout << Param["L"][0] << std::endl; 
 std::map <std::string, std::string> save;
 std::pair<std::string, std::string> s;
	
 s.first = "E";
 s.second = s.first + "_" + std::to_string(int(Param["ID"][0]));
 save.insert(s);
	
 s.first = "Folder";
 s.second = "res_";
 save.insert(s);
 
 AperiodicDisorder GenDis(Param["L"][0], "aa");
 std::pair<std::string, Array_d> param;
 param.first = "Js";
 param.second = GenDis.generate();
 Param.m.insert(param);
 
 ITensorDMRG Simul = ITensorDMRG();
 Simul.Init(Param, save);
 Simul.build_H();
 Simul.set_initial_state();
 Simul.run();
 
 Simul.observables(save);
 Simul.save_observables(save);
 
 MPI_Finalize();
 return 0;
}