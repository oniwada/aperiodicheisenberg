#!/bin/bash
#$ -cwd
#$ -S /bin/bash
#$ -o Adapt.o
#$ -e Adapt.e

make runAH
time /opt/openmpi/bin/mpirun -n 2 --machinefile hosts ./runAH Results.db Results Inputs.txt Results/res_
