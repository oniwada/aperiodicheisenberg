#ifndef _GENERATE_DISORDER_
#define _GENERATE_DISORDER_

class AperiodicDisorder{
private:
	std::string disorder;
public:
	uint64_t L;
	
	AperiodicDisorder(int L, std::string init):
		disorder(init){
		if(init.size()%2 == 1){
			std::cout << "initial condition length must be even" << std::endl;
			exit(1);
		}
		this->L = L;
	}
	
	std::vector<double> generate(void){
		while(disorder.size() < L){
			iterate();
		}
		return get_disorder();
	}
	
	void iterate(void){
		std::string next_dis;
		for(uint64_t i = 0; i < disorder.size(); i+=2UL){
			std::string pair = disorder.substr(i, 2);
			if( pair == "aa" ){
				next_dis += "aabaababba";
			}
			else if( pair == "ab"){
				next_dis += "aabaab";
			}
			else if( pair == "ba"){
				next_dis += "abbaaaabba";
			}
			else{
				std::cout << "pair " << pair << " is not aa, ab or ba" << std::endl;
				exit(1);
			}
		}
		disorder.swap(next_dis);
	}
	
	std::vector<double> get_disorder(void){
		std::vector<double> v;
		for(int i = 0; i < L; i++)
			v.push_back(disorder[i] == 'a' ? 0.0 : 1.0);
		return v;
	}
};

#endif