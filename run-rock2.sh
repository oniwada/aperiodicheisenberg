#!/bin/bash
#$ -cwd
#$ -S /bin/bash
#$ -o Adapt.o
#$ -e Adapt.e
#PBS -l nodes=compute-0-2

make runSpinHalfHeisenberg
time ./runSpinHalfHeisenberg
