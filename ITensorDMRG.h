#ifndef __ITENSORDMRG__
#define __ITENSORDMRG__

#include <map>
#include "Parameters.h"

using Array_d = std::vector<double>;
using Parameter  = std::pair<std::string, Array_d>;

class ITensorDMRG{
	public:
 Parameters<double> Param;
 itensor::Real energy;
 std::vector<itensor::MPS> psi_0;
 std::vector<itensor::MPS> psi_perp;
 std::vector<itensor::SpinHalf> sites;
 itensor::MPO H;
 std::map<std::string, Array_d> Obs;
 std::map<std::string, std::string> save_name;
 DouToStr<double> D2S;
	
 ITensorDMRG(){}
 
 virtual ITensorDMRG operator=(Parameters<double> &inputs){
		return ITensorDMRG();
 }
 
 virtual ITensorDMRG& Init(Parameters<double> &inputs, 
						   std::map<std::string, std::string> save_name){
	Param = inputs;
	this->save_name = save_name;
	
	std::string s_file = save_name["Folder"]+"sites_"+std::to_string(int(Param["ID"][0]));
	std::ifstream f(s_file);
	if(f.good()){
		itensor::SpinHalf s;
		itensor::readFromFile(s_file, s);
		sites.push_back(s);
	}
	else{
		itensor::SpinHalf s;
		s = itensor::SpinHalf(Param["L"][0]);
		sites.push_back(s);
	}
	
	std::pair<std::string, Array_d> tempS;
	tempS.first = "S";
	tempS.second = Array_d(unsigned(Param["L"][0]-1), 0.0);
	Obs.insert( tempS);
	
	std::pair<std::string, Array_d> tempE;
	tempE.first = "E";
	tempE.second = Array_d(2, 0.0);
	Obs.insert( tempE);
	
	std::pair<std::string, Array_d> tempMLD;
	tempMLD.first = "MaxLinkDim";
	tempMLD.second = Array_d(2, 0.0);
	Obs.insert( tempMLD);	
	return *this;
 }
 
 virtual ITensorDMRG& build_H(){
	auto auto_ampo = itensor::AutoMPO(sites[0]);
	for(int i = 1; i < Param["L"][0]; i++){
		double J = get_J(i-1);
		double D = Param["Delta"][0];
		auto_ampo += 1.0*J*D,"Sz",i,"Sz",i+1;
		auto_ampo += 0.5*J  ,"S+",i,"S-",i+1;
		auto_ampo += 0.5*J  ,"S-",i,"S+",i+1;
	}
	H = itensor::toMPO(auto_ampo);
	return *this;
 }
 
 double get_J(int i){
	 if( Param["Js"][i] == 0){
		return Param["Jb"][0]*(1.0-Param["r"][0]);
	 }
	 else{
		return Param["Jb"][0];
	 }
 }
 
 virtual ITensorDMRG& set_initial_state(){

	std::ifstream p_file(save_name["Folder"]+
					 "psi_0_"+
					 std::to_string(int(Param["ID"][0])));
	if(p_file.good()){
		loadPsiFromFile();
		return *this;
	}

	auto state = itensor::InitState(sites[0]);
	for(int i = 1; i <= Param["L"][0]; i++){
		if( (i+2)%2 )
			state.set(i, "Up");
		else
			state.set(i, "Dn");
	}
	psi_0.push_back(itensor::MPS(state));
	
	auto state1 = itensor::InitState(sites[0]);
	for(int i = 1; i <= Param["L"][0]; i++){
		if( (i+2)%2 or i == 1)
			state1.set(i, "Up");
		else
			state1.set(i, "Dn");
	}
	psi_0.push_back(itensor::MPS(state1));
	return *this;
 }
 
 void loadPsiFromFile(void){
	for(int i = 0; i < 2; i++){
		itensor::MPS temp;
		itensor::readFromFile(save_name["Folder"]+
							 "psi_"+std::to_string(i)+"_"+
							 std::to_string(int(Param["ID"][0])), temp);
		psi_0.push_back(temp);
	}
 }
 
 virtual ITensorDMRG& run(){
	itensor::Sweeps sweeps  = build_sweeps();
	itensor::Sweeps sweeps2  = build_sweeps();
	
	auto [energy_, psi_] = itensor::dmrg(H, psi_0[0], sweeps, {"Silent", true});
	Obs["E"][0] = energy_;
	psi_perp.push_back(psi_);
	
	auto [energy_1, psi_1] = itensor::dmrg(H, 
	                                       psi_perp, 
										   psi_0[1], 
										   sweeps2,
										   {"Silent", true,"Weight=",Param["L"][0]});
	Obs["E"][1] = energy_1;
	psi_perp.push_back(psi_1);
	
	for(int i = 0; i < psi_perp.size(); i++){
		Obs["MaxLinkDim"][i] = itensor::maxLinkDim(psi_perp[i]);
		if ( itensor::maxLinkDim(psi_perp[i]) >= Param["max_bond_dim"][0] ){
			std::cout << "maximum bond dimension reached!!!" << std::endl;
		}
	}
	
	return *this;
 }
 
 virtual ITensorDMRG& observables(std::map<std::string, std::string> save_name){
	if(save_name.find("S") != save_name.end()){
		for(int i = 1; i < Param["L"][0]; i++){
			Obs["S"][i-1] += EntanglementEntropy( i, psi_perp[0]);
		}
	}
	if(save_name.find("E") != save_name.end()){
		//already saved in method run
	}
	if(save_name.find("MaxLinkDim") != save_name.end()){
		//already saved in method run
	}
	return *this;
 }
 
 std::string save_observables(std::map<std::string, std::string> save_name){
	std::string save_str;
	for(auto &s: save_name){
		save_str += "#";
		save_str += s.second + "\n";
		int i = 0;
		for(; i < Obs[s.first].size(); i++){
			save_str += std::to_string(i+1) + " " + D2S.d2s(Obs[s.first][i]) + "\n";
		}
		save_str += std::to_string(i+1) + " " + D2S.d2s(Param["NDis"][0]) + "\n";
	}
	itensor::writeToFile(save_name["Folder"]+"sites_"+std::to_string(int(Param["ID"][0])), sites[0]);
	for(int i = 0; i < psi_perp.size(); i++){
		itensor::writeToFile(save_name["Folder"]+
		                     "psi_"+std::to_string(i)+"_"+
							 std::to_string(int(Param["ID"][0])), psi_perp[i]);
	}
	return save_str;
 }
 
 itensor::Sweeps build_sweeps(void){
	itensor::Sweeps sweeps = itensor::Sweeps(int(Param["NDis"][0]));
	sweeps.maxdim() = Param["max_bond_dim"][0];
	sweeps.mindim() = Param["min_bond_dim"][0];
	sweeps.cutoff() = Param["t_error"][0];
	sweeps.noise() = Param["noise"][0];
	return sweeps;
//	int i = 0;
//	for(i = 0; i < Param["max_bond_dim"].size(); i++){
//		sweeps.setmaxdim(i, int(Param["max_bond_dim"][i]));
//	}
//	sweeps.setmaxdim(i, int(Param["max_bond_dim"].back()));
//	
//	for(i = 0; i < Param["min_bond_dim"].size(); i++){
//		sweeps.setmindim(i, int(Param["min_bond_dim"][i]));
//	}
//	sweeps.setmindim(i, int(Param["min_bond_dim"].back()));
//	sweeps.mindim() = Param["min_bond_dim"][0];
	
//	for(i = 0; i < Param["t_error"].size(); i++){
//		sweeps.setcutoff(i, int(Param["t_error"][i]));
//	}
//	sweeps.setcutoff(i, int(Param["t_error"].back()));
//	
//	for(i = 0; i < Param["noise"].size(); i++){
//		sweeps.setnoise(i, int(Param["noise"][i]));
//	}
//	sweeps.setnoise(i, int(Param["noise"].back()));
 }
 
 double EntanglementEntropy(int i, itensor::MPS &psi){
	psi.position(i);
	itensor::Index l_link = itensor::leftLinkIndex(psi, i);
	itensor::Index s = itensor::siteIndex(psi, i);

	auto [U, S, V] = itensor::svd(psi(i), {l_link, s});
	itensor::Index c_index = itensor::commonIndex(U, S);
	 
	double entropy = 0.0;
	for( auto n : itensor::range1(itensor::dim(c_index))){
		double rho3 = itensor::elt(S, n, n);
		entropy += VnEntropy( rho3*rho3);
	}
	return entropy;
 }

 double VnEntropy(double lambda){
	if(fabs(lambda) < 1e-12)
		return 0.0;
	else
		return -lambda*log(lambda);
 }
 
 double correlation(int i, int j, const std::string op, itensor::MPS &psi){
	if ( i == j){
		psi.position(i);
		itensor::ITensor psi_d = itensor::dag(itensor::prime(psi(i), "Site"));
		itensor::ITensor psi_i = psi(i);
		itensor::ITensor Sz = sites[0].op(op, i);
		return itensor::elt( itensor::noPrime(psi_i*Sz)*Sz*psi_d );
	}
	else if( i > j){
		return correlation(j, i, op, psi);
	}

	psi.position(i);
	auto Sz_i = sites[0].op(op, i);
	itensor::MPS psi_d = itensor::dag(psi);
	psi_d.prime("Link");
	
	auto i_link = itensor::leftLinkIndex(psi, i); 
	auto corr = itensor::prime( psi(i), i_link);
	corr *= Sz_i;
	corr *= itensor::prime(psi_d(i), "Site");
	 
	i++;
	for(; i < j; i++){
		corr *= psi(i);
		corr *= psi_d(i);
	}
	 
	auto Sz_j = sites[0].op(op, j);
	auto j_link = itensor::rightLinkIndex(psi, j);
	corr *= itensor::prime(psi(j), j_link);
	corr *= Sz_j;
	corr *= itensor::prime(psi_d(j), "Site");
	return itensor::elt(corr);
 }

 double average(int i, const std::string op, itensor::MPS &psi){
	psi.position(i);
	itensor::ITensor psi_d = itensor::dag(itensor::prime(psi(i), "Site"));
	itensor::ITensor psi_i = psi(i);
	itensor::ITensor Sz = sites[0].op(op, i);
	return itensor::elt( psi_i*Sz*psi_d );
 }
 
 void Print_inputs(){
	for(auto &m : Param.m){
		std::cout << m.first << ": " << m.second[0] << std::endl;
	}
 }
};

#endif