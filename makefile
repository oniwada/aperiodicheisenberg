include ../ITensor/options.mk

LIB_DIR=../SQLiteAmalg/sqlite -I../ITensor -I../GeneralMonteCarlo
LIB_DIR+=$(ITENSOR_INCLUDEFLAGS)
LINK_DIR=../ITensor/lib 
LINK_DIR+=$(ITENSOR_LIBFLAGS)

CFLAGS= -O2 -std=c++17 -m64 -fconcepts -fPIC -D__SQLITEAMALGAMATION__

STUFF=-litensor -lpthread -lblas -llapack -lz -ldl -lm -Wl,-rpath -Wl,/usr/lib/x86_64-linux-gnu/hdf5/serial -lhdf5 -lhdf5_hl 

CC=mpiCC
OBJ=../SQLiteAmalg/sqlite/sqlite3.o

all: runAH
	
runAH: runAH.cpp ITensorDMRG.h
	@clear
	@clear
	$(CC) $(CFLAGS) -I$(LIB_DIR) -L$(LINK_DIR) -o $@ $< $(OBJ) $(STUFF)

Param_test: Param_test.cpp
	@clear
	@clear
	$(CC) $(CFLAGS) -I$(LIB_DIR) -L$(LINK_DIR) -o $@ $< $(OBJ) $(STUFF)

runSpinHalfHeisenberg: runSpinHalfHeisenberg.cpp ITensorDMRG.h
	@clear
	@clear
	$(CC) $(CFLAGS) -I$(LIB_DIR) -L$(LINK_DIR) -o $@ $< $(OBJ) $(STUFF)
	
clean:
	rm main runSpinHalfHeisenberg runAH Param_test
