#include <iostream>
#include <iomanip> 
#include <vector>
#include <algorithm>
#include <map>

#include <mpi.h>
#include <omp.h>

#include "itensor/all.h"
#include "ITensorDMRG.h"

#include "Parameters.h"
#include "_Database_.h"
#include "_LoadData_.cpp"
#include "_MPI_string_.cpp"
#include "GenerateDisorder.h"

void master(int rank, int size, 
            std::string file, 
			std::vector<std::string> PNames, 
			std::string DatabaseName, 
			std::string tableName){

 _Database_<double> Data(DatabaseName.c_str(), tableName.c_str());

 time_t tt = time(NULL);
 std::mt19937_64 rnd(tt);
 std::uniform_int_distribution<unsigned int> dist(0, ~0);

 MPI_string seed(" ");
 for(int i = 1; i < size; i++){
	seed.str.clear();
	seed.str = std::to_string(dist(rnd));
	seed.Send(i, 0);
 }

 std::ifstream data(file);
 MPI_string param(" ");
 MPI_string buffer(" ");
 while( std::getline(data, param.str) ){
	Parameters<double> p(param.str, PNames);
	Data.InitParameters(p);
	param.str += " "+std::to_string(p["ID"][0]);

	time_t t1, t2;

	buffer.str.clear();
	buffer.SleepRecvAny();
	if( buffer.str.find("IDLE") == 0 ){
		param.Send(buffer.Status.MPI_SOURCE, 0);
	}
	else if(buffer.str.size() == 0){
		param.Send(buffer.Status.MPI_SOURCE, 0);
		continue;
	}
	else{
		time(&t1);
		UpdateFile<double>f;
		f.Update(buffer.str);
		
		if(buffer.str.size() != 0){
			Data.Database.Exec("BEGIN TRANSACTION;");
			Data.Database.Exec(buffer.str);
			Data.Database.Exec("END TRANSACTION;");
		}
		time(&t2);
		std::cout<< "time to save" << difftime(t1, t2) << std::endl;
		param.Send(buffer.Status.MPI_SOURCE, 0);
	}
 }
 param.str = "STOP";
 while(size !=1){
	size--;
	buffer.str.clear();
	buffer.SleepRecvAny();
	if( buffer.str.find("IDLE") == 0){
		param.Send(buffer.Status.MPI_SOURCE, 0);
	}
	else if(buffer.str.size() == 0){
		param.Send(buffer.Status.MPI_SOURCE, 0);
		continue;
	}
	else{
		time_t t1, t2;
		time(&t1);
		UpdateFile<double>f;
		f.Update(buffer.str);
		
		if(buffer.str.size() != 0){
			std::cout << "*" << buffer.str << "*" << std::endl;
			Data.Database.Exec("BEGIN TRANSACTION;");
			Data.Database.Exec(buffer.str);
			Data.Database.Exec("END TRANSACTION;");
		}
		
		time(&t2);
		std::cout<< "time to save" << difftime(t1, t2) << std::endl;
		param.Send(buffer.Status.MPI_SOURCE, 0);
	}
 }
}

void slave(int rank, int size, std::vector<std::string> &PNames, std::string FolderFileName){

 PNames.push_back("ID");
 
 MPI_string p("IDLE");
 std::uniform_real_distribution<double> dist(0.0, 1.0);

 MPI_string seed(" ");
 seed.Recv(0, 0);
 std::mt19937_64 rnd((unsigned int)std::stol(seed.str));

 int xxx = rnd();
 std::cout << "seed " <<xxx <<std::endl;
 
 std::vector<itensor::MPS> carry_psi;
 std::vector<itensor::SpinHalf> sites;

 while(true){

	p.Send(0, 0);
	p.Recv(0, 0);

	if( p.str.find("STOP") == 0){
		break;
	}

	Parameters<double> Param(p.str, PNames);

	if (Param["NDis"][0] ==0){
		p.str.clear();
		p.str = "IDLE";
		continue;
	}
	
	std::map <std::string, std::string> save;
	std::pair<std::string, std::string> s;
	s.first = "S";
	s.second = "O_"+FolderFileName + s.first + "_" + std::to_string(int(Param["ID"][0]));
	save.insert(s);
	
	s.first = "E";
	s.second = "A_"+FolderFileName + s.first + "_" + std::to_string(int(Param["ID"][0]));
	save.insert(s);
	
	s.first = "MaxLinkDim";
	s.second = "A_"+FolderFileName + s.first + "_" + std::to_string(int(Param["ID"][0]));
	save.insert(s);
	
	s.first = "Folder";
	s.second = FolderFileName;
	save.insert(s);
	
	AperiodicDisorder GenDis(Param["L"][0], "aa");
	std::pair<std::string, Array_d> param;
	param.first = "Js";
	param.second = GenDis.generate();
	Param.m.insert(param);
	
	ITensorDMRG Simul = ITensorDMRG();
	Simul.Init(Param, save);

	std::ifstream p_file(save["Folder"]+
				 "psi_0_"+
				 std::to_string(int(Param["ID"][0])));
	if(p_file.good() or carry_psi.size() == 0){
		Simul.set_initial_state();
	}
	else{
		Simul.psi_0.swap(carry_psi);
		Simul.sites.swap(sites);
	}
	Simul.build_H();
	Simul.run();
	Simul.observables(save);

	
	p.str.clear();
	p.str = Simul.save_observables(save);
	
	carry_psi.swap(Simul.psi_perp);
	sites.swap(Simul.sites);
 }
}

int main(int Nargs, char* Inputs[]){

 std::mt19937 rng(time(NULL));
 std::uniform_real_distribution<double> dist(0., 1.);

 int rank, size, thread_level;
 MPI_Init_thread(&Nargs, &Inputs, MPI_THREAD_FUNNELED, &thread_level);
 MPI_Comm_rank(MPI_COMM_WORLD, &rank);
 MPI_Comm_size(MPI_COMM_WORLD, &size);
 
 std::cout << "OpenMPrunning with " << omp_get_max_threads() << "threads" << std::endl;

 std::vector<std::string> PNames;
 PNames.push_back("L");
 PNames.push_back("n_sweeps");
 PNames.push_back("max_bond_dim");
 PNames.push_back("min_bond_dim");
 PNames.push_back("noise");
 PNames.push_back("t_error");
 PNames.push_back("Jb");
 PNames.push_back("r");
 PNames.push_back("Delta");
 PNames.push_back("NDis");

 if (rank == 0){
	master(rank, size, Inputs[3], PNames, Inputs[1], Inputs[2]);
 }
 else{
	slave(rank, size, PNames, Inputs[4]);
 }
 std::cout << "rank = " << rank << std::endl;
 MPI_Finalize();
 return 0;
}